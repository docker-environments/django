# Django development Environment

## Setup

1. Clone repo
1. Review the requirements.txt file for packages to be installed
1. Build and start the project by using:
   
   ```
   ./build.sh
   docker-compose up -d
   ```
   The containers have short names (web & db) to use with the `docker` command. This might collide with names of other containers on your machine.
   
1. Init a django project in the current folder (substitute 'demoprj' for your own project name):

	```
   docker-compose run --rm web django-admin startproject config /code/
   ```
   It's a good idea to name the initial project "config", since it will mainly cvontain the config stuff for all the other apps in the project.
   
1. Edit PROJECTNAME/settings.py and enter the database settings:
   (side note: this setup currently doesn't work on M1 Macs so I'm falling back to SQLite)

    ```
    DATABASES = {
      'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'pgpw',
        'HOST': 'db',
        'PORT': 5432,
      }
    }
    ```
1. Run the initial migration creating the admin database stuff:

   ```
   docker-compose run --rm web python manage.py migrate
   ```
1. Create a superuser to log into the admin area (need to use 'docker exec -it' because it's interactive):

   ```
   docker exec -it web python manage.py createsuperuser
   ```
    

## Some useful commands

when your django environment is hosted in a docker container, you're not able to issue django just at the command line. You have to wrap them up in a docker command launching a shell command inside your container. To clean up after running the command, we use ```--rm``` to dispose the container running the shell command. For the sake of simplicity I assume your project is called ```demoprj``` and your app is called ```demoapp```.

1. Init a new project:

   ```
   docker-compose run --rm web django-admin.py startproject demoprj .
   ```
1. Start a new app:

   ```
   docker-compose run --rm web python manage.py startapp demoapp
   ```
   (Don't forget to add your app to the INSTALLED_APPS in ```demoprj/settings.py```)
1. Install additionally required Python packages:

   ```
   docker exec web pip install -r requirements.txt
   ```
1. Build a migration after changing the model for app demoapp:

   ```
   docker-compose run --rm web python manage.py makemigrations demoapp
   ```
1. Running the migration against the database:

   ```
   docker-compose run --rm web python manage.py migrate
   ```
1. Create a superuser for the admin area:

   ```
   docker exec -it web python manage.py createsuperuser
   ```
1. Open the Python shell:

   ```
   docker exec -it web python manage.py shell
   ```

## References

https://docs.docker.com/compose/django/<br/>
https://django-extensions.readthedocs.io/en/latest/index.html
