FROM python:3.7-bullseye

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && apt-get install -y vim lsof less graphviz
RUN apt-get install -y --no-install-recommends postgresql-client && rm -rf /var/lib/apt/lists/*

RUN mkdir /code
WORKDIR /code

ADD ./code/ /code/
RUN pip install --upgrade pip && pip install -r requirements.txt

EXPOSE 8000
